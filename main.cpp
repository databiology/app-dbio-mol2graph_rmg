//
//  main.cpp
//  mol2graph
//
//  Created by Yuliia Orlova on 15/09/16.
//  Copyright © 2016 Yuliia Orlova. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

string input_file_name, output_file_name_1, output_file_name_2;

int N_atoms = 0;
int N_edges = 0;
string* atoms;
int** A;

void read_input_file(string input_file_name)
{
    
    ifstream inFile;
    inFile.open(input_file_name.c_str());
    
   // if (!inFile.is_open())
   //     throw logic_error( "File <" + input_file_name + " cannot open the file." );

    string data;
    getline( inFile, data );
    getline( inFile, data );
    //getline( inFile, data );

    
    inFile >> N_atoms;
    getline(inFile, data);
    
    atoms = new string[N_atoms];
    
    A = new int*[N_atoms];
    for (int i = 0; i < N_atoms; i++)
        A[i] = new int[N_atoms];
    
    for (int i = 0; i< N_atoms; i++)
        for (int j = 0; j<N_atoms; j++)
            A[i][j] = 0;
    
    
    for (int i = 0; i < N_atoms; ++i){
        double dummy_number = 0;
        //char atom;
    string radical, connectivity, dummy_string, atom;
        inFile >> dummy_number >> atom >> radical >> dummy_string >> dummy_string;
        getline( inFile, connectivity );
        
        int N_spaces = 0;
        char nextChar;
        // checks each character in the string
        for (int j=0; j<int(connectivity.length()); j++)
        {
            nextChar = connectivity.at(j); // gets a character
            if (isspace(connectivity[j]))
                N_spaces++;
        }
        
        
        
        istringstream iss(connectivity);
        
        for (int j = 0; j<N_spaces; j++){
            string sub_line;
            char bond;
            iss >> sub_line;
            char sub_line_number[sub_line.length()-4];
            for (int k = 1; k<(sub_line.length()-3); k++){
                sub_line_number[k-1] = sub_line.at(k);
            }
            int index = atoi(sub_line_number);
            bond = sub_line.at(sub_line.length()-2);
            int t =0;
            t = index;
            if (bond =='S'){
            A[i][t-1] = 1;
                A[t-1][i] = 1;
            }
            else if (bond =='D'){
                A[i][t-1] = 2;
                A[t-1][i] = 2;

            }
            
            
        }
        atoms[i] = atom;
        if (radical == "u1"){
            A[i][i] = 1;
            atoms[i].append(".");
        }
        
        
    
    }

    
//    for (int i = 0; i< N_atoms; i++){
//        for (int j = 0; j<N_atoms; j++){
//            cout<<A[i][j]<<" ";
//        }
//        cout<<endl;
//    }
    
    inFile.close();
};

void write_output_file(string output_file_name_1, string output_file_name_2){

    ofstream output_file_1;
    ofstream output_file_2;
    //cout<<"inside the function"<<endl;
    output_file_1.open(output_file_name_1.c_str());
    output_file_2.open(output_file_name_2.c_str());
    
    if (output_file_1.is_open()==0){
        cout<<"error: could not create output file 1!"<<endl;
        exit(1);
    }

    if (output_file_2.is_open()==0){
        cout<<"error: could not create output file 2!"<<endl;
        exit(1);
    }
    for (int i = 0; i < N_atoms; i++){
        output_file_1<<atoms[i]<<endl;}
    for (int i = 0; i<N_atoms; i++){
        for (int j = 0; j< N_atoms; j++){
                output_file_2<<A[i][j]<<" ";
        }
        output_file_2<<endl;}

    output_file_1.close();
    output_file_2.close();
};


int main(int argc, const char * argv[]) {
    
    if(argc!=4){
        std::cout<<"error: wrong invocation!"<<endl;
        std::cout<<"try with:"<<endl;
        std::cout<<"motion <string input_file_name>"<<endl;
        return 0;
        
    }
    input_file_name=argv[1]; // file EL.txt
    output_file_name_1 = argv[2]; //file atoms_EL.txt - list of atom of the input molecule
    output_file_name_2 = argv[3]; // file adjacency_EL.txt - connectivity matrix of the input molecule 

   // example of output:
   // output_file_name_1="atoms_molecule_EL.txt";
   // output_file_name_2 = "adjacency_molecule_EL.txt";

    
    read_input_file(input_file_name );
    write_output_file(output_file_name_1, output_file_name_2);
    
    
    return 0;
}


